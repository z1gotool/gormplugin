package main

import (
	"gitee.com/z1gotool/gormplugin/z1add_field"
	"gitee.com/z1gotool/gormplugin/z1util"
	"gitee.com/z1gotool/z1err"
	"gorm.io/gorm"
)

func main() {
	// https://github.com/go-gorm/gorm/issues/3565
	// https://juejin.cn/post/7131258433948352520

	var db = &gorm.DB{}

	err := db.Use(&z1add_field.Z1AddField{})
	z1err.Check(err)

	db = z1util.UpdateDBContext(
		// `z1AddFieldInsert`,
		`z1AddFieldWhere`,
		map[string]interface{}{
			`place_id`: 73501,
		},
		db,
		false,
	)

	_ = db

}
