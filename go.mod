module gitee.com/z1gotool/gormplugin

go 1.19

require (
	gitee.com/z1gotool/z1err v1.0.0
	gorm.io/gorm v1.21.16
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
